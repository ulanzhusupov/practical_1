import "dart:io";

void main() {
  print("Введите градусы цельсия, которые нужно перевести в фаренгейты:");
  var a = stdin.readLineSync()!;

  double farengate = double.parse(a) * (9/5) + 32;

  print("Градусы цельсия $a по Фаренгейту будет: $farengate");
}