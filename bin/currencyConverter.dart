import "dart:io";


void main() {
  String currencies = """
1. USD (покупка: 88.25, продажа: 88.44)
2. EUR (покупка: 93.90, продажа: 94.90)
3. RUB (покупка: 0.890, продажа: 0.919)
4. KZT (покупка: 0.1302, продажа: 0.2017)
""";
  print("Введите сумму, которую нужно поменять на другую валюту:");
  var summ = stdin.readLineSync()!;

  print("Введите название валюты, на который нужно сконвертировать (USD, EUR, RUB, KZT):");
  String currencyName = stdin.readLineSync()!;

  print("Введите стоимость этой валюты:");
  var price = stdin.readLineSync()!;

  
  double converted = double.parse(summ) / double.parse(price);

  print("Ваша сумма: ${summ}KGS.\nВы получили: $converted $currencyName.");

  // if(int.parse(currencyChoice) == 1) {
  //   converted = double.parse(summ) / 88.25;
  //   print("Ваша сумма: ${summ}KGS.\nВы получили: $converted USD.");

  // } else if(int.parse(currencyChoice) == 2) {
  //   converted = double.parse(summ) / 93.90;
  //   print("Ваша сумма: ${summ}KGS.\nВы получили: $converted EUR.");

  // } else if(int.parse(currencyChoice) == 3) {
  //   converted = double.parse(summ) / 0.890;
  //   print("Ваша сумма: ${summ}KGS.\nВы получили: $converted RUB.");

  // } else if(int.parse(currencyChoice) == 4) {
  //   converted = double.parse(summ) / 0.1302;
  //   print("Ваша сумма: ${summ}KGS.\nВы получили: $converted KZT.");
  // }
}